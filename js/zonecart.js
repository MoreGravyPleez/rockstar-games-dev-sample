// JavaScript Document
// simple shopping cart plugin using cookies - Chris Sealy
(function( $ ) {
	$.fn.zoneCart = function (options) {
		//define the caller object
		var elem = $(this);
		
		//set some defaults
		var defaults = {
            cartArea: 0,
			bigCart: 0,
			miniCartArea: 0,
            shopArea: 0,
            placeInCart: 0,
			destroy: 0
        };
        var options = $.extend(defaults, options);
		
		//Parse the XML containing the items and load into memory
		$.ajax({
			type:"GET",
			url:"./gamedata.xml",
			dataType:"xml",
			timeout: (2 * 1000),
			success: function(xml) {
				dataXML = xml;
				if(options.bigCart != 0){
					buildCartPage(options.bigCart);	
				}
				buildCart(options.cartArea, options.miniCartArea);
			},
			error: function( objAJAXRequest, strError ){alert("Error! Type: " + strError);
			}
		});
		
		
		
		//capture all "add to cart" clicks on the page and get their associated game id's
		//then add the item to the cart
		$(options.shopArea).find('.product').find(options.placeInCart).click(function(e){
			e.preventDefault();
			addItem($(this).parents('.product').attr('game-id'));
		});
		
		$(options.shopArea).find('.title').find(options.destroy).click(function(e){
			e.preventDefault();
			emptyCart();
		});
		
		var dataXML = null;
		var title = "";
		var short_txt = "";
		var description = "";
		
		
		
		
		
		//add item to the cart
		function addItem(itemID) {
			//Parse the XML cached data
			if(dataXML != null){
				$(dataXML).find('item').each(function(){
					//console.log($(this).attr('id') + "-from XML file");
					if($(this).attr('id') == itemID){
						title = $(this).find('name').text();
						short_txt = $(this).find('short').text();
						description = $(this).find('description').text();
					}
				});	
			}
						
			//using cookies, so we need to check if the cookie exists, if not we create it once we parse
			//the data and build and array
			if($.cookie('zonecart')){
				var cArray = stringToArray($.cookie('zonecart'));
			} else {
				var cArray = new Array();
				var alreadyInCart = false;
			}
			//console.log("cArray contents when we check for cookie --> " + cArray);
			
			
			if(cArray.length == 0){
				var arrayItems = [itemID, title, 1];
				cArray.push(arrayItems);
				//console.log("if cArray.length is 0 --> " + cArray);
			}else if (cArray.length > 0) {
				for(i=0; i<cArray.length; i++){
					if(cArray[i][0] == itemID){
						alreadyInCart = true;
						currIndex = i;
						break;
					} else {
                        alreadyInCart = false;
                    }
				}
				if(alreadyInCart){
					var amt = cArray[currIndex][2];
					cArray[currIndex][2] = ++amt;
					//console.log("if alreadyInCart is TRUE --> " + cArray);
				}else {
					var arrayItems = [itemID, title, 1];
					cArray.push(arrayItems);
					//console.log("if alreadyInCart is FALSE --> " + cArray);
				}
			}
			
			$.cookie("zonecart", cArray.join("|"), { path: "/" });
			//alert($.cookie("zonecart"));
			buildCart(options.cartArea, options.miniCartArea);

		};
		
		function buildCart(area, miniArea) {
			var counter = 0;
			if ($.cookie('zonecart') && $.cookie('zonecart') != "") {
				var cArray = stringToArray($.cookie('zonecart'));
				$(area).html('')
				$(miniArea).html('');
				var strOutput = '<table class="cartContents">';
				strOutput = strOutput + '<tr><td>Item ID</td><td>Title</td><td>Quantity</td></tr>';
				for (var i = 0; i < cArray.length; i++) {
					strOutput = strOutput + '<tr><td>' + cArray[i][0] + '</td>';
					strOutput = strOutput + '<td>' + cArray[i][1] + '</td>';
					strOutput = strOutput + '<td>' + cArray[i][2] + '</td>';
					strOutput = strOutput + '</tr>';
					counter = counter + eval(cArray[i][2]);
				}
				strOutput = strOutput + '</table>';
				strOutput = strOutput + '<a href="" class="closeCart">Close Cart Window</a>'
				$(area).append(strOutput); //populate
				strOutput = ""; //clear temp variable to build drawer contents
				strOutput = '<h3>Mini Cart</h3><div>';
				strOutput = '<table class="minicart">';
				strOutput = strOutput + '<tr><td>Title</td><td>Quan.</td></tr>';
				for (var i = 0; i < cArray.length; i++) {
					strOutput = strOutput + '<tr><td>' + cArray[i][1] + '</td>';
					strOutput = strOutput + '<td>' + cArray[i][2] + '</td>';
					strOutput = strOutput + '</tr>';
				}
				strOutput = strOutput + '</table></div>';
				$(miniArea).append(strOutput); //populate
			} else {
				$(area).html("Cart is empty!");	
				$(miniArea).html("Cart is empty");
			}
			$('div.counter').html('You have ' + counter + ' items in your cart');
		}
		
		function buildCartPage(page) {
			var counter = 0;
			if ($.cookie('zonecart') && $.cookie('zonecart') != "") {
				var cArray = stringToArray($.cookie('zonecart'));
				$(page).html('')
				var strOutput = '<table class="cartWide">';
				strOutput = strOutput + '<tr><td>Item ID</td><td>Title</td><td>Description</td><td>Quantity</td></tr>';
				for (var i = 0; i < cArray.length; i++) {
					if(dataXML != null){
						$(dataXML).find('item').each(function(){
							if($(this).attr('id') == String(cArray[i][0])){
								description = $(this).find('description').text();
							}
						});	
					}
					strOutput = strOutput + '<tr><td>' + cArray[i][0] + '</td>';
					strOutput = strOutput + '<td>' + cArray[i][1] + '</td>';
					strOutput = strOutput + '<td>' + description + '</td>';
					strOutput = strOutput + '<td>' + cArray[i][2] + '</td>';
					strOutput = strOutput + '</tr>';
					counter = counter + eval(cArray[i][2]);
				}
				strOutput = strOutput + '</table>';
				$(page).append(strOutput); //populate
				
			} else {
				$(page).html("Cart is empty!");	
			}
		}
		
		function emptyCart() {
           	$.cookie('zonecart', null, { path: "/" });
            buildCart(options.cartArea, options.miniCartArea);
        }
		
		function stringToArray(string) {
            if (string) {
                y = [];
                if (string.indexOf("|") != -1) {
                    x = string.split("|");
                    for (var i in x) {
                        y.push(x[i].split(","))
                    }
                } else {
                    y.push(string.split(","))
                }
                return y
            } else {
                return false
            }
        }
		
	};
	
})(jQuery);
jQuery.cookie = function (name, value, options) {
    if (typeof value != 'undefined') {
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000))
            } else {
                date = options.expires
            }
            expires = '; expires=' + date.toUTCString()
        }
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('')
    } else {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break
                }
            }
        }
        return cookieValue
    }
};